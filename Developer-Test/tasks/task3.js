// possible test cases:
// [1,0] => [1,1]
// [1,1] => [1,0,0]
// [1,1,0] => [1,1,1]
// .......
// [1,0,0,0,0,0,0,0,0,1] => [1,0,0,0,0,0,0,0,1,0]

const binary = [];

// binary representation
function binaryModifier(bin) {
    for (let i = 0; i < bin.length; i++) {
        if (bin[i] === 0) {
            binary.push(bin[i]);
        }

        if (bin[i] === 1) {
            binary.push(bin[i]);
        }
    }
}

console.log(binaryModifier(process.argv[2]));
