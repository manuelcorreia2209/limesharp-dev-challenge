//const example = ([1,2,3]);

function valueIterator(array) {
    const normalizedArray = JSON.parse(array);
    return Array(normalizedArray.length).fill(normalizedArray).flat()
}

console.log(valueIterator(process.argv[2]));
