//const example = "liMeSHArp DeveLoper TEST";

function stringModifier(string) {
    const removeVowels = string.replace(/[aeiou]/gi, '');
    return (removeVowels.charAt(0).toUpperCase() + removeVowels.slice(1).toLowerCase());
}

console.log(stringModifier(process.argv[2]));
